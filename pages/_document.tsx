import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
    render() {
        return (
            <html lang='en'>
            <Head>
                <title>Merqurion</title>
                <meta name="description" content="Merqurion Solutions" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
            </html>
        )
    }
}
