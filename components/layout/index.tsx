import Navbar from './navbar'
import Banner from './banner'
import About from './about'
import Service from './service'
import Client from './client'
import Portfolio from './portfolio'
import Contact from './contact'

export {Navbar, Banner, About, Service, Client, Portfolio, Contact}