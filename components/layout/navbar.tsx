import styles from '../../../styles/Navbar.module.scss'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons"; // import the icons you need

export default function Navbar() {
  var active_menu = 'home';

  const menuClicked = (e: any, menu: string) => {
    e.preventDefault();

    active_menu = menu;
    setActiveMenu();
  }

  const setActiveMenu = () => {
    var activated = document.getElementsByClassName('active-menu');
    var activated_mobile = document.getElementsByClassName('active-menu-mobile');

    if(activated.length>0) 
      activated[0].className = '';
    
    if(activated_mobile.length>0) 
      activated_mobile[0].className = '';
    

    document.getElementById(`menu-${active_menu}`).className = 'active-menu';
    document.getElementById(`menu-mobile-${active_menu}`).className = 'active-menu-mobile';
  }

  const showMenuMobile = (e) => {
    e.preventDefault();
    document.getElementById('menus-mobile-button').style.display = 'none';
    document.getElementById('menus-mobile').style.display = 'block';

    setActiveMenu();
  }

  const closeMenuMobile = (e) => {
    e.preventDefault();
    document.getElementById('menus-mobile-button').style.display = 'block';
    document.getElementById('menus-mobile').style.display = 'none';
  }

  return <div id="navbar">
    <div>
      <a id='logo' href=''>
        <img src='assets/logo.png'/>
        <div id='name'>
          <h1>merqurion</h1>
          <h2>solutions</h2>
        </div>
      </a>
      <div id='menus'>
        <ul>
          <li id='menu-home' className='active-menu'><a href='' onClick={(e) => menuClicked(e, "home")}>Home</a></li>
          <li id='menu-about'><a href='' onClick={(e) => menuClicked(e, "about")}>About us</a></li>
          <li id='menu-service'><a href='' onClick={(e) => menuClicked(e, "service")}>Services</a></li>
          <li id='menu-client'><a href='' onClick={(e) => menuClicked(e, "client")}>Client</a></li>
          <li id='menu-portfolio'><a href='' onClick={(e) => menuClicked(e, "portfolio")}>Portfolio</a></li>
          <li id='menu-contact'><a href='' onClick={(e) => menuClicked(e, "contact")}>Contact</a></li>
        </ul>
      </div>
      <div id='menus-mobile'>
        <ul>
          <li id='menu-mobile-close'><a href='' onClick={(e) => closeMenuMobile(e)}><FontAwesomeIcon icon={faTimes}></FontAwesomeIcon></a></li>
          <li id='menu-mobile-home' className='active-menu-mobile'><a href='' onClick={(e) => menuClicked(e, "home")}>Home</a></li>
          <li id='menu-mobile-about'><a href='' onClick={(e) => menuClicked(e, "about")}>About us</a></li>
          <li id='menu-mobile-service'><a href='' onClick={(e) => menuClicked(e, "service")}>Services</a></li>
          <li id='menu-mobile-client'><a href='' onClick={(e) => menuClicked(e, "client")}>Client</a></li>
          <li id='menu-mobile-portfolio'><a href='' onClick={(e) => menuClicked(e, "portfolio")}>Portfolio</a></li>
          <li id='menu-mobile-contact'><a href='' onClick={(e) => menuClicked(e, "contact")}>Contact</a></li>
        </ul>
      </div>
      <span id='menus-mobile-button' onClick={(e) => showMenuMobile(e)}>
        <FontAwesomeIcon icon={faBars}></FontAwesomeIcon>
      </span>
    </div>
  </div>
}